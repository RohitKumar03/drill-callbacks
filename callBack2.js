const listData = require('./data/lists.json')
/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed
     to it from the given data in lists.json.
     Then pass control back to the code that called it by using a callback function.
     
*/

const getListInformation = (listData,passID,callBack)=>{

    if(typeof listData !== 'object' || typeof callBack !=='function'|| typeof passId === undefined){
        throw new Error('Invalid input type arguments')
    }
    
    setTimeout(() => {
        for(let key in listData){
            if(key ===passID){
               return  callBack(listData[key])
            }
        }
        
    }, 2*1000);
}

module.exports = {getListInformation, listData}