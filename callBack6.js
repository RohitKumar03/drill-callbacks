/* 
	Problem 6: Write a function that will use the previously written functions to get the following information.
     You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/
const { getBoardInformation, boardData } = require("./callBack1.js");
const { getListInformation, listData } = require("./callBack2.js");
const { getCardInformation, cardData } = require("./callBack3");

const callBackHellThree = (boardData, listData, cardData) => {
  setTimeout(() => {
    getBoardInformation(boardData, "Thanos", (boardData) => {
      console.log(boardData);
      getListInformation(listData, boardData[0]["id"], (listData) => {
        console.log(listData);
        listData.forEach((element) => {
          getCardInformation(cardData, element["id"], (cardData) => {
            console.log(cardData);
          });
        });
      });
    });
  }, 2 * 1000);
};
module.exports = { callBackHellThree, boardData, cardData, listData };
