/* 
	Problem 4: Write a function that will use the previously written functions to get the following information.
     You do not need to pass control back to the code that called it.

    Get information from
the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
const { getBoardInformation, boardData } = require("./callBack1.js");
const { getListInformation, listData } = require("./callBack2.js");
const { getCardInformation, cardData } = require("./callBack3");

const getInformation = (boardData, listData, cardData) => {
  return setTimeout(() => {
    getBoardInformation(boardData, "Thanos", (callBackOne) => {
      console.log(callBackOne);

      getListInformation(listData, callBackOne[0]["id"], (callBackTwo) => {
        console.log(callBackTwo);

        let specificProperty = callBackTwo.filter((element) => {
          return element["name"] === "Mind";
        });
        getCardInformation(
          cardData,
          specificProperty[0]["id"],
          (callBackThree) => {
            console.log(callBackThree);
          }
        );
      });
    });
  }, 2 * 1000);
};
// getInformation(boardData, listData,cardData);
 module.exports = { getInformation, cardData, boardData, listData };
