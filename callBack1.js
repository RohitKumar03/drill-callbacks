const boardData = require("./data/boards.json");

/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed 
    from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const getBoardInformation = (boardData, passId, callBack) => {
  if (
    typeof boardData !== "object" ||
    typeof callBack !== "function" ||
    typeof passId === undefined
  ) {
    throw new Error("Invalid input type arguments");
  }
  
  setTimeout(() => {
    let getBoardData = boardData.filter((element) => {
      return (element["id"] === passId || element['name'] ==='Thanos');
    });
    return callBack(getBoardData);
  }, 2 * 1000);
};

module.exports = { getBoardInformation, boardData };
