const cardData = require('./data/cards.json')

/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID 
    that is passed to it from the given data in cards.json. Then pass control back to the code that called it by 
    using a callback function.
*/

const getCardInformation = (cardData,passId, callBack)=>{
    if(typeof cardData !=='object' || typeof passId ==='undefined' || typeof callBack !=='function'){
        throw new Error('Invalid input type argument')
    }
    setTimeout(() => {
    for(const [key,value] of Object.entries(cardData)){
        if(key === passId){
            return callBack(value)
        }
    }
        
    }, 2*1000);
}

module.exports = {getCardInformation,cardData}
